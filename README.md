## English
### About
Hypersocial parse feeds and post them to GNUSocial or Pump.io, and it is based on gnurss and spigot. The idea of the gnusrss program came from spigot, a program that posts feeds to the social network pump.io as does gnusrss (and hypersocial) but better, because it controls the possible flood. hypersocial does not have this option and it will be managed with the crontab (for now).

### Features

* Multiple feed and GNU Social accounts support
* sqlite3 is used to store the feeds
* Can fetch RSS files or url indistinctly
* Twitter image upload support when used with twitter2rss

### Requirements
Need a version equal to or greater than python 3 and some libraries:

* feedparser>= 5.0
* requests>= 2.11.1
* pypump>=0.6
* argparse>=1.0
    
### Install

As with any program that uses python, it should be used a virtual environment (virtualenv), but that is user selectable. It's possible to use one of the next installation methods:

Clone the repository:

    $ git clone https://git.hyperbola.info:50100/services/hypersocial.git
    $ cd hypersocial
    $ su -c "pip3 install -r requirements.txt"
    $ su -c "python3 setup.py install"

### Configuration
The program is (or should be) quite intuitive. Running the following, should show the basic.
    
#### For GNUSocial:

    $ hypersocial-gs.py
    usage: hypersocial [-h] [-c file_name] [-C] [-p config_file] [-P] [-k file_name]

    Post feeds to GNU Social

    optional arguments:
        -h, --help            show this help message and exit
	-c file_name, --create-config file_name
                        creates a config file
	-C, --create-db       creates the database
	-p config_file, --post config_file
                        posts feeds
	-P, --post-all        posts all feeds
	-k file_name, --populate-database file_name
                        fetch the RSS and save it in the database

    
In any case, if not clear, read the following.

For the first use, it must be created the database and the first configuration file. This can done using the same command, like this:


    $ hypersocial-gs.py --create-db --create-config hypersocial

Then it will ask several questions to create the first configuration file. It should look like this:

     Database created!
     Hi! Now we'll create config file!
     Please enter the feed's URL: https://www.hyperbola.info/feeds/news/
     Please enter your username (user@server.com): hypersocial@quitter.cl
     Please enter your password: yourPassword
     Do you need to shorten the URLs that you 'post? Please take in account
     That You should only use it if your node only have 140 characters.
     Answer with "yes" or just press enter if you do not want to use it:
     Please enter your feed's fallbackurl. If you do not want or have one,
     just press enter:
     Now we're gona fetch the feed. Please wait ...
     Done! The tags are:
        tags
        title_detail
	link
	authors
	links
	author_detail
	published_parsed
	title
	summary
	id
	author
	published
	guidislink
	summary_detail
     The XML has-been parsed. Choose wich format you want:
     Please put the tags inside the square brackets
     Ex: {title} - {link} by @{author}: {title} - {link} by @{author}
     Do you want to allow insecure connection to your GNU social server?
     Answer with "yes" or just press enter if you don't want to use it:
     Do you want to populate the database? (y) Or you prefer to post old items? (n)

The file is saved under the name 'hypersocial.ini'. It should look like this:

    [Feeds]
    feed = https://www.hyperbola.info/feeds/news/
    user = hypersocial@quitter.cl
    password = yourPassword
    shorten =
    fallback_feed =
    format = {title} - {link} by @ {author}

It can create all the configuration files you want.
When creating the above file, it put into the database all the feeds that had so far. Thus, when running *hypersocial* for the first time, it will not post nothing to GNU Social until the feed has new information.
To post feeds from a concrete config file or all execute, respectively, the following:

    $ hypersocial-gs.py -p hypersocial.ini
    $ hypersocial-gs.py -P

If the config file is created manually and the user don't want to post all the feed's content, just use the --populate-database option to save them to the database:

    $ hypersocial-gs.py -k otherFile.ini

#### For Pump.io
    
    $ hypersocial-pump.py --help
    usage: hypersocial-pump.py [-h] [--version] [--add-feed]
                           [--log-level {DEBUG,INFO,WARNING,ERROR,CRITICAL}]

    optional arguments:
    -h, --help            show this help message and exit
    --version, -v
    --add-feed, -f
    --log-level {DEBUG,INFO,WARNING,ERROR,CRITICAL}, -l {DEBUG,INFO,WARNING,ERROR,CRITICAL}

To configure hypersocial-pump.py for first use, run it from the command-line:
    
    $ hypersocial-pump.py
    No configuration file now, running welcome wizard.
    2017-06-10 18:07:00,145 WARNING: Could not load configuration file
    Adding feed...
    Feed URL: https://www.hyperbola.info/feeds/news/
    Account Webfinger ID (e.g. bob@example.com): hypersocial@datamost.com
    Please follow the instructions at the following URL:
    https://datamost.com/oauth/authorize?oauth_token=oC7M2iYilZI-XqF6CBSAFw
    Verifier: theVerifierCode
    Hyperbola RSS feeds
    Minimum time between posts (minutes): 30
    Hypersocial formats your outgoing posts based on fields in the feed
                being scanned. Specify the field name surrounded by the '%'
                character to have it replaced with the corresponding value for
                the item (e.g. %title% or %link%).
    The following fields are present in an example item in
                        this feed:
    title
    title_detail
    links
    link
    summary
    summary_detail
    authors
    author
    author_detail
    published
    published_parsed
    id
    guidislink
    Next you will be prompted for the message format and
                optional title format for outgoing posts.
    Message format: %summary%
    Title format (optional): %title%

You can see an example of this here https://datamost.com/hypersocial/note/qT0BSiLwSj2ta70cZ7fxCA
After that, everytime you run hypersocial-pump.py feeds will automatically be posted on all the accounts you've set up. Running without specifying any options will result in no console output unless there are warnings or errors. This is optimal for running hypersocial as a cron job. To view more verbose logging, you can specify the --log-level option.
    
The credentials for the accounts are delegated to PyPump, and they are stored in ~/.config/PyPump/credentials.json

### Crontab
The recommended way to execute hyperbola is using the crontab. Each time it's run, a single item of the feed will be posted to prevent flood. Depending on the number of feeds that are published, you should choose a different runtime. For a blog that publishs once a day, it could be used the following:

    $ crontab -e
    # A file is open and gets the following
    * 12 * * * cd /path/to/hypersocial && hypersocial-gs.py -p hypersocial.ini && hypersocial-pump.py

So it runs once, every day at midday. If, however, hypersocial-gs it's used with twitter2rss, it could be recommended putting it to run every five minutes. It has to be remembered that is important to run in the directory where the database was created, because is where it will search it..

### Use hypersocial-gs with twitter2rss and/or GNU Social
It works like any feed, except for the field that is published. In both you have to choose ={summary}=. An example configuration file is as follows:

    [feeds]
    feed = https://quitter.se/api/statuses/user_timeline/127168.atom
    user = drymer@quitter.se
    password = falsePassword
    shorten =
    fallback_feed =
    format = {summary}

The feed can be achieved by looking at the source code of the page of the account you want. For twitter2rss, you can host it or can use this web http://daemons.cf/twitter2rss
    
## Castellano
### Acerca de
hypersocial parsea feeds y los postea en GNUSocial, y está basado en gnusrss y spigot. La idea de hacer el programa gnusrss surgió de spigot, un programa que postea feeds en la red social pump.io igual que hace gnusrss (e hypersocial) pero mejor, ya que controla el posible flood. hypersocial no tiene esta opción y se controlará con el propio crontab (de momento).

### Características
* Soporta múltiples feeds y cuentas de GNU Social
* sqlite3 es usado para guardar los feeds
* Se puede usar tanto archivos RSS cómo url indistintamente
* Soporta la súbida de imágenes de Twitter cuando es usado en conjunto con twitter2rss

### Requisitos
Necesita una versión de python igual o superior a la 3 y algunas librerias:
* feedparser>= 5.0
* requests>= 2.11.1
* pypump>=0.6
* argparse>=1.0

### Instalación
Cómo con cualquier programa con python, es recomendable usar un entorno virtual (virtualenv), pero eso queda a elección del usuario. Se puede escoger entre los siguientes metodos:

Clonar el repositorio:

      $ git clone https://git.hyperbola.info:50100/services/hypersocial.git
      $ cd hypersocial
      $ su -c "pip3 install -r requirements.txt"
      $ su -c "python3 setup.py install"
    
### Configuración
    El programa es (o debería ser) bastante intuitivo. Ejecutando lo siguiente, deberia verse lo básico.
    
#### Para GNUSocial

    $ hypersocial-gs.py
    usage: hypersocial [-h] [-c file_name] [-C] [-p config_file] [-P] [-k file_name]

Post feeds to GNU Social

    optional arguments:
        -h, --help            show this help message and exit
	-c file_name, --create-config file_name
                        creates a config file
	-C, --create-db       creates the database
	-p config_file, --post config_file
                        posts feeds
	-P, --post-all        posts all feeds
	-k file_name, --populate-database file_name
                        fetch the RSS and save it in the database

En cualquier caso, si no queda claro, leer lo siguiente.

Para el primer uso, la base de datos y el primer archivo de configuración deben ser creados. Podemos hacerlo usando la misma orden, tal que así:

    $ hypersocial-gs.py --create-db --create-config hypersocial

A continuación hará varias preguntas para configurar el primer archivo de configuración. Debería verse así:

     Database created!
     Hi! Now we'll create de config file!
     Please introduce the feed's url: https://www.hyperbola.info/feeds/news/
     Please introduce your username (user@server.com): hypersocial@quitter.cl
     Please introduce your password: tuContraseña
     {1}Do you need to shorten the urls that you post? Please take in account
     that you should only use it if your node only has 140 characters.
     Answer with "yes" or just press enter if you don't want to use it:
     {2}Please introduce your feed's fallbackurl. If you don't want or have one,
     just press enter:
     Now we're gona fetch the feed. Please wait...
     Done! The tags are:
        tags
	title_detail
	link
	authors
	links
	author_detail
	published_parsed
	title
	summary
	id
	author
	published
	guidislink
	summary_detail
     The XML has been parsed. Choose wich format you want:
     {3}Please put the tags inside the square brackets
     Ex: {title} - {link} by @{author}: {title} - {link} by @{author}
     {4}Do you want to allow insecure connection to your GNU social server?
     Answer with "yes" or just press enter if you don't want to use it:
     {5}Do you want to populate the database? (y) Or you prefer to post old items? (n)

A continuación traduciré las lineas con los números entre corchetes.
{1} Necesitas acortar las url que quieres postear? Por favor ten en cuenta que sólo deberia usarse si el servidor sólo tiene 140 carácteres.
{2} Por favor introduce tu feed de emergencia. Si no tienes uno, solamente aprieta enter.
{3} Por favor pon las etiquetas dentro de los corchetes.
{4} Quieres permitir conexiones inseguras a tu servidor GNU social? Responde con "si" o simplemente apreta enter si no necesitas usarlo.
{5} Quieres llenar la base de datos? (y) O prefieres publicar los artículos antiguos? (n)

Respecto al 3, hay que examinar el código fuente del RSS para saber cuales interesan. En general, el que hay de ejemplo será lo que se busque. En el caso 4, sólo es útil si el servidor usa un certificado auto-firmado.

El archivo se guardará con el nombre 'hypersocial.ini'. Después de todas estas preguntas, debería verse similar a esto:

    [feeds]
    feed = https://www.hyperbola.info/feeds/news/
    user = hypersocial@quitter.cl
    password = tuContraseña
    shorten =
    fallback_feed =
    format = {title} - {link} by @{author}
    insecure =

Se pueden crear todos los archivos de configuración que se quieran.
Al haber creado el archivo anterior, se han metido en la base de datos todos los feeds que habian hasta el momento. Por lo tanto, cuando se ejecuta *hypersocial* por primera vez, no posteará nada en GNU Social, a menos que el feed tenga nueva información.
    Para postear los feeds de un archivo o todos, ejecutar, respectivamente, lo siguiente:

    $ hypersocial-gs.py -p hypersocial.ini
    $ hypersocial-gs.py -P

Si el archivo de configuración ha sido creado manualmente y no se quiere postear el contenido del feed, sólo hay que ejecutar la opción --populate-database para guardar estos en la base de datos:

    $ hypersocial-gs.py -k otherFile.ini
    
#### Para Pump.io
    
    $ hypersocial-pump.py --help
    usage: hypersocial-pump.py [-h] [--version] [--add-feed]
                           [--log-level {DEBUG,INFO,WARNING,ERROR,CRITICAL}]

    optional arguments:
    -h, --help            show this help message and exit
    --version, -v
    --add-feed, -f
    --log-level {DEBUG,INFO,WARNING,ERROR,CRITICAL}, -l {DEBUG,INFO,WARNING,ERROR,CRITICAL}
    
Para configurar hypersocial-pump.py para su primer uso, ejecútalo desde la línea de comandos:
    
    $ hypersocial-pump.py
    No configuration file now, running welcome wizard.
    2017-06-10 18:07:00,145 WARNING: Could not load configuration file
    Adding feed...
    Feed URL: https://www.hyperbola.info/feeds/news/
    Account Webfinger ID (e.g. bob@example.com): hypersocial@datamost.com
    Please follow the instructions at the following URL:
    https://datamost.com/oauth/authorize?oauth_token=oC7M2iYilZI-XqF6CBSAFw
    Verifier: theVerifierCode
    Hyperbola RSS feeds
    Minimum time between posts (minutes): 30
    Hypersocial formats your outgoing posts based on fields in the feed
                being scanned. Specify the field name surrounded by the '%'
                character to have it replaced with the corresponding value for
                the item (e.g. %title% or %link%).
    The following fields are present in an example item in
                        this feed:
    title
    title_detail
    links
    link
    summary
    summary_detail
    authors
    author
    author_detail
    published
    published_parsed
    id
    guidislink
    Next you will be prompted for the message format and
                optional title format for outgoing posts.
    Message format: %summary%
    Title format (optional): %title%
    
Puedes ver un ejemplo de esto en https://datamost.com/hypersocial/note/qT0BSiLwSj2ta70cZ7fxCA
Después, cada vez que ejecutes hypersocial-pump.py los feeds se postearán automáticamente en todas las cuentas que hayas configurado. Ejecutarlo sin  especificar alguna opción resultará en ninguna salida de consola a no ser que hayan advertencias o errores. Esto es óptimo para ejecutarlo como un cron job. Para ver un registro más verboso, usted puede especificar --log-level OPCIÓN.
    
Los credenciales para las cuentas están delegadas a PyPump, y están almacenados en ~/.config/PyPump/credentials.json

### Crontab
El modo recomendado de ejecución de hypersocial es usando el crontab. Cada vez que se ejecute posteará un sólo elemento del feed para evitar el flood. Según la cantidad de feeds que se publiquen, se deberia escoger un tiempo de ejecución distinto. Para un blog que publique una vez al día, con poner lo siguiente, deberia valer:

    $ crontab -e
    # Se abrirá un archivo y se mete lo siguiente
    * 12 * * * cd /ruta/de/hypersocial && hypersocial-gs.py -p hypersocial.ini && hypersocial-pump.py

Así se ejecuta una vez al día, a las doce del mediodía. Si, en cambio, lo usasemos con twitter2rss, se recomienda poner que se ejecute cada cinco minutos. Hay que recordar que es importante que se ejecute en el directorio en el que se ha creado la base de datos, ya que es ahí dónde la buscará.
    
### Uso de hypersocial-gs con twitter2rss y/o GNU Social
Funciona igual que con cualquier feed, exceptuando el campo que se publica. En ambos hay que escoger ={summary}=. Un ejemplo de archivo de configuración sería el siguiente:

    [feeds]
    feed = https://quitter.se/api/statuses/user_timeline/127168.atom
    user = drymer@quitter.se
    password = contraseñaFalsa
    shorten =
    fallback_feed =
    format = {summary}

El feed se puede conseguir mirando el código fuente de la página de la cuenta que se quiere. En el caso de twitter2rss, se puede hostear o se puede usar esta web http://daemons.cf/twitter2rss . No es recomendable usarlo con ningún nodo que use 140 carácteres, ya que en los retweets se añade un símbolo, "♻", lo cual hará que un tweet de 140 carácteres no sea posteado.
