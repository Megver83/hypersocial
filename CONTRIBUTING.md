# Contributing
*Hypersocial* is made by the [Hyperbola Project](https://www.hyperbola.info/).<br>
Everyone is invited to contribute to the Hyperbola GNU/Linux-libre developement and related projects.

If you which to help the development of *Hypersocial*, you are free to do by [emailing me](mailto:megver83@hyperbola.info) so you have access to the git repository. Note that as the Hyperbola Project is in favor of decentralization, its developers also develop their software in a federalized way. This repository is maintained in [GitLab](https://gitlab.com/Megver83/hypersocial) and has a mirror at the [Hyperbola's Git Repositories](https://git.hyperbola.info:50100/community/hypersocial.git/).
