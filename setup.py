#!/usr/bin/env python

from setuptools import setup

VERSION = '0.2.3'

setup(name='hypersocial',
      version=VERSION,
      description='Post feeds to GNU Social & Pump.io.',
      long_description=open('README.md',"rb").read().decode('utf8'),
      author='Megver83',
      author_email='megver83@hyperbola.info',
      url='https://git.hyperbola.info:50100/services/hypersocial.git/   ',
      download_url='https://gitlab.com/Megver83/hypersocial/repository/' + VERSION + '/archive.tar.gz',
      scripts=['hypersocial-gs.py',
               'hypersocial-pump.py'],
      license="GPLv3",
      install_requires=[
          "feedparser>=5.0",
          "requests>=2.11.1",
          "pypump>=0.6",
          "argparse>=1.0",
          ],
      classifiers=["Development Status :: 5 - Production/Stable",
                   "Programming Language :: Python",
                   "Programming Language :: Python :: 3",
                   "Programming Language :: Python :: 3.4",
                   "Operating System :: OS Independent",
                   "Operating System :: POSIX",
                   "Intended Audience :: End Users/Desktop"]
      )
